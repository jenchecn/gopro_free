package route

import (
	"server/app/baisc/files"
	"server/app/baisc/menu"
	"server/app/baisc/role"
	"server/app/baisc/user"
	"server/middleware"

	"github.com/gin-gonic/gin"
)

func Init(app *gin.Engine) {

	v1 := app.Group("/v1")
	{
		//白名单内无需验证token
		WhiteList := v1.Group("")
		{
			WhiteList.GET("/getCaptcha", middleware.GetCaptchaHandler)
			WhiteList.POST("/password_login", middleware.PasswordHandler)
			WhiteList.GET("/password_login_out", middleware.PasswordLoginOut)
			WhiteList.GET("/userinfo", middleware.GetUserInfo)
		}
		//总平台模块路由
		Basic := v1.Group("/basic", middleware.AuthMiddleware)
		{
			User := Basic.Group("/user")
			{
				User.GET("/list", user.GetList)
				User.POST("/add", user.Add)
				User.POST("/edit", user.Edit)
				User.GET("/del", user.Del)
				User.GET("/one", user.GetOne)
				User.POST("/change_pass", user.ChnagePass)

			}
			Menu := Basic.Group("/menu")
			{
				Menu.GET("/menu_tree", menu.GetMenuTree)
				Menu.GET("/list", menu.GetList)
				Menu.POST("/add", menu.Add)
				Menu.POST("/edit", menu.Edit)
				Menu.GET("/one", menu.GetOne)
				Menu.POST("/add_all", menu.AddAll)
			}
			Role := Basic.Group("/role")
			{
				Role.GET("/list", role.GetList)
				Role.POST("/add", role.Add)
				Role.POST("/edit", role.Edit)
				Role.GET("/del", role.Del)
				Role.GET("/one", role.GetOne)
			}
			Files := Basic.Group("/files")
			{
				Files.POST("/upload", files.Upload)
				Files.POST("/uploads", files.Uploads)
			}
		}
	}
}
