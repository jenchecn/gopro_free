/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 8.0.12 : Database - appointment


/*Data for the table `menus` */

INSERT  INTO `menus`(`id`,`status`,`is_delete`,`created_at`,`updated_at`,`parent_id`,`type`,`menu_name`,`route_path`,`permission`,`assembly_path`,`icon`,`sort`) VALUES 
(1,0,0,1636963614,1637732923,0,0,'首页','/home','home','','el-icon-s-home',0),
(2,0,0,1636963614,1640143632,0,0,'总平台','/system','system','','el-icon-setting',0),
(3,0,0,1637025447,1637025447,2,1,'菜单管理','/system/menu','system.menu','system/menu/index','el-icon-s-platform',0),
(4,0,0,1637025541,1637025541,2,1,'用户管理','/system/user','system.user','system/user/index','el-icon-user-solid',0),
(5,0,0,1637032076,1637032076,3,2,'查询',NULL,'system.menu.query','','',0),
(6,0,0,1637032076,1637032076,3,2,'新增',NULL,'system.menu.add',NULL,NULL,0),
(7,0,0,1637032076,1637032076,3,2,'编辑',NULL,'system.menu.edit',NULL,NULL,0),
(8,0,0,1637032076,1637032076,3,2,'删除',NULL,'system.menu.del',NULL,NULL,0),
(9,0,0,1637032076,1637032076,4,2,'查询',NULL,'system.user.query',NULL,NULL,0),
(10,0,0,1637047920,1637047920,4,2,'新增','','system.user.add','','',0),
(11,0,0,1637048219,1637048219,4,2,'编辑','','system.user.edit','','',0),
(12,0,0,1637048292,1637048292,4,2,'删除','','system.user.del','','',0),
(13,0,0,1638870073,1638870073,4,2,'设置角色','','system.user.set_role','','',0),
(14,0,0,1638870200,1638870200,4,2,'禁用/启用','','system.user.status','','',0),
(15,0,0,1637303072,1637303072,2,1,'角色管理','/system/role','system.role','system/role/index','el-icon-phone',0),
(16,0,0,1637547411,1637547411,15,2,'查询','','system.role.query','','',0),
(17,0,0,1637547451,1637547451,15,2,'新增','','system.role.add','','',0),
(18,0,0,1637547467,1637547467,15,2,'编辑','','system.role.edit','','',0),
(19,0,0,1637547562,1637547562,15,2,'删除','','system.role.del','','',0);

/*Data for the table `roles` */
INSERT  INTO `roles`(`id`,`status`,`is_delete`,`created_at`,`updated_at`,`role_name`) VALUES 
(1,0,0,1640588439,1640588439,'超级管理员');

/*Data for the table `role_permissions` */
INSERT  INTO `role_permissions`(`id`,`status`,`is_delete`,`created_at`,`updated_at`,`role_id`,`permission`) VALUES 
(1,0,0,1640588439,1640588439,1,'home'),
(2,0,0,1640588439,1640588439,1,'system'),
(3,0,0,1640588439,1640588439,1,'system.menu'),
(4,0,0,1640588439,1640588439,1,'system.menu.query'),
(5,0,0,1640588439,1640588439,1,'system.menu.add'),
(6,0,0,1640588439,1640588439,1,'system.menu.edit'),
(7,0,0,1640588439,1640588439,1,'system.menu.del'),
(8,0,0,1640588439,1640588439,1,'system.user'),
(9,0,0,1640588439,1640588439,1,'system.user.query'),
(10,0,0,1640588439,1640588439,1,'system.user.add'),
(11,0,0,1640588439,1640588439,1,'system.user.edit'),
(12,0,0,1640588439,1640588439,1,'system.user.del'),
(13,0,0,1640588439,1640588439,1,'system.user.set_role'),
(14,0,0,1640588439,1640588439,1,'system.user.status'),
(15,0,0,1640588439,1640588439,1,'system.role'),
(16,0,0,1640588439,1640588439,1,'system.role.query'),
(17,0,0,1640588439,1640588439,1,'system.role.add'),
(18,0,0,1640588439,1640588439,1,'system.role.edit'),
(19,0,0,1640588439,1640588439,1,'system.role.del');

/*Data for the table `users` */
INSERT  INTO `users`(`id`,`status`,`is_delete`,`created_at`,`updated_at`,`account`,`password`,`phone`,`nick_name`,`avatar`,`birthday`,`sex`,`email`,`role_id`,`store_id`,`last_login_ip`,`last_login_time`) VALUES 
(1,1,0,1643161094,1643161094,'admin','e10adc3949ba59abbe56e057f20f883e','13000000000','超级管理员','upload/img/default.jpg','2022-01-26 09:38:14.5951301 +0800 CST m=+0.018566701',0,'490912587@qq.com',1,0,'127.0.0.1',0);