module server

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.4
	github.com/go-redis/redis/v8 v8.11.4
	github.com/kr/text v0.2.0 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/shopspring/decimal v1.3.1
	github.com/spf13/afero v1.6.0
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/wenlng/go-captcha v1.0.7
	golang.org/x/crypto v0.0.0-20211202192323-5770296d904e // indirect
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v2 v2.4.0
	gorm.io/driver/mysql v1.1.3
	gorm.io/gorm v1.22.2
)
