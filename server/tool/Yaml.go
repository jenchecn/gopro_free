package tool

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
)

type Model struct {
	App struct {
		Host string `yaml:"host"`
		Port string `yaml:"port"`
	}
	Os struct {
		DoMain string `yaml:"domain"`
	}
	Mysql struct {
		Host     string `yaml:"host"`
		Port     string `yaml:"port"`
		User     string `yaml:"user"`
		Password string `yaml:"password"`
		DbName   string `yaml:"dbname"`
	}
	Jwt struct {
		AppKey string `yaml:"appkey"`
		Expire int64  `yaml:"expire"`
	}
	Md5 struct {
		Hash string `yaml:"hash"`
	}
	Debug        bool `yaml:"debug"`
	Wechatapplet struct {
		AppID  string `yaml:"appid"`
		Secret string `yaml:"secret"`
	}
}

func Config() Model {
	config := Model{}
	content, err := ioutil.ReadFile("./.yaml")
	if err != nil {
		log.Fatalf("解析config.yaml读取错误: %v", err)
	}
	if yaml.Unmarshal(content, &config) != nil {
		log.Fatalf("解析config.yaml出错: %v", err)
	}
	return config
}
