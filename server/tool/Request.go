package tool
type Pagination  struct {
	PageNo int
	PageSize int
	KeyWords string
	DescAsc string
	OrderBy string
	Type int
}