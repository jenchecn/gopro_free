package tool

import (
	"crypto/md5"
	"encoding/hex"
)

//md5加密
func Md5(src string) string {
	//hash := Config().Md5.Hash
	m := md5.New()
	m.Write([]byte(src))
	//res := hex.EncodeToString(m.Sum([]byte(hash)))
	res := hex.EncodeToString(m.Sum(nil))
	return res
}
