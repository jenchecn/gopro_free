/**
 * @Author: 490912587@qq.com
 * @Description:
 * @File:  Cache
 * @Version: 1.0.0
 * @Date: 2021/12/8 11:43
 */
package tool

import (
	"context"
	"github.com/go-redis/redis/v8"
	"time"
)

var ctx = context.Background()

func Conn() (*redis.Client, error) {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379",
		Password: "",
		DB:       0,
	})
	_, err := rdb.Ping(ctx).Result()
	if err != nil {
		return nil, err
	} else {
		return rdb, nil
	}
}
func SetValue(redis *redis.Client, key string, value string) error {
	expire := Config().Jwt.Expire
	_, err := redis.Set(ctx, key, value, time.Duration(expire)*time.Second).Result()
	if err != nil {
		return err
	} else {
		return nil
	}
}
func GetValue(redis *redis.Client, key string) (string, error) {
	result, err := redis.Get(ctx, key).Result()
	if err != nil {
		return "", err
	} else {
		return result, nil
	}
}
func DelCache(redis *redis.Client, key string) (int64, error) {
	result, err := redis.Del(ctx, key).Result()
	if err != nil {
		return 0, err
	} else {
		return result, nil
	}
}
