package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"server/database"
	"server/middleware"
	"server/route"
	"server/tool"
)

func main() {
	// 初始化gin框架
	app := gin.Default()

	// 配置静态文件夹路径
	app.StaticFS("/public", http.Dir("./public"))

	// 商城管理，应用管理，文件中心暂时不做
	config := tool.Config()
	gin.SetMode(gin.DebugMode)
	if !config.Debug {
		//设置方式
		gin.SetMode(gin.ReleaseMode)
	}
	//初始化数据库
	database.Init()

	//允许前端跨域，日志中间件，异常处理返回500不会服务关闭
	app.Use(middleware.Cors())

	//初始化路由
	route.Init(app)
	//最大上传文件大小
	app.MaxMultipartMemory = 8 // 8 MiB

	//if !config.Debug {
	//	logfile := "./logs/" + strconv.FormatInt(time.Now().Unix(), 10) + ".log"
	//	path, err := os.Create(logfile)
	//	gin.DefaultWriter = io.MultiWriter(path, os.Stdout)
	//	if err != nil {
	//		panic(err)
	//	}
	//	defer path.Close()
	//	app.Use(gin.LoggerWithWriter(path))
	//}
	app.Use(gin.Recovery())
	app.Run(config.App.Host + ":" + config.App.Port)
}
