package user

import (
	"net/http"
	"server/database/model"
	"server/tool"
	"strconv"

	"github.com/gin-gonic/gin"
)

var user model.User

func GetList(ctx *gin.Context) {
	keywords := ctx.Query("keywords")
	page, _ := strconv.Atoi(ctx.Query("page"))
	size, _ := strconv.Atoi(ctx.Query("size"))
	result, err := user.GetList(
		tool.Pagination{
			PageNo:   page,
			PageSize: size,
			KeyWords: keywords,
		})
	if err == nil {
		ctx.JSON(http.StatusOK, gin.H{"code": 200, "msg": "获取成功！", "list": result})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"code": 201, "msg": "数据为空！"})
	}
}
func GetOne(ctx *gin.Context) {
	id, _ := strconv.Atoi(ctx.Query("id"))
	result, err := user.GetOne(uint(id))
	if err == nil {
		ctx.JSON(http.StatusOK, gin.H{"code": 200, "msg": "获取成功！", "data": result})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"code": 201, "msg": "数据为空！"})
	}
}
func Add(ctx *gin.Context) {
	user = model.User{}
	user.Account = ctx.PostForm("account")
	user.Avatar = ctx.PostForm("avatar")
	user.Birthday = ctx.PostForm("birthday")
	user.Email = ctx.PostForm("email")
	user.LastLoginIp = ctx.PostForm("last_login_ip")
	user.NickName = ctx.PostForm("nick_name")
	user.Password = ctx.PostForm("password")
	user.Phone = ctx.PostForm("phone")
	roleId, _ := strconv.Atoi(ctx.PostForm("role_id"))
	user.RoleId = roleId
	user.Status = 1 //1为启用 2禁用 0默认
	err := user.Add(user)
	if err == nil {
		ctx.JSON(http.StatusOK, gin.H{"code": 200, "msg": "添加成功！"})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"code": 201, "msg": "添加失败"})
	}
}
func Edit(ctx *gin.Context) {
	id, _ := strconv.Atoi(ctx.PostForm("id"))
	user = model.User{}
	user.ID = uint(id)
	user.Account = ctx.PostForm("account")
	user.Avatar = ctx.PostForm("avatar")
	user.Birthday = ctx.PostForm("birthday")
	user.Email = ctx.PostForm("email")
	user.LastLoginIp = ctx.PostForm("last_login_ip")
	user.NickName = ctx.PostForm("nick_name")
	user.Password = ctx.PostForm("password")
	user.Phone = ctx.PostForm("phone")
	roleId, _ := strconv.Atoi(ctx.PostForm("role_id"))
	//gorm 坑：status设置默认值后不能修改回默认值，默认是0只能修改为1或2，不能修改为0
	status, _ := strconv.Atoi(ctx.PostForm("status"))
	user.Status = status
	user.RoleId = roleId
	err := user.Edit(user)
	if err == nil {
		ctx.JSON(http.StatusOK, gin.H{"code": 200, "msg": "修改成功！"})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"code": 201, "msg": "修改失败"})
	}
}
func Del(ctx *gin.Context) {
	id, _ := strconv.Atoi(ctx.PostForm("id"))
	err := user.Del(uint(id))
	if err == nil {
		ctx.JSON(http.StatusOK, gin.H{"code": 200, "msg": "删除成功！"})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"code": 201, "msg": "删除失败"})
	}
}
func ChnagePass(ctx *gin.Context) {
	id, _ := strconv.Atoi(ctx.PostForm("id"))
	password := ctx.PostForm("password")
	newpassword := ctx.PostForm("newpassword")
	result, finderr := user.ChangePass(uint(id), password)
	if finderr == nil {
		result.Password = newpassword
		err := user.Edit(result)
		if err == nil {
			ctx.JSON(http.StatusOK, gin.H{"code": 200, "msg": "密码修改成功,下次登录生效！"})
		} else {
			ctx.JSON(http.StatusOK, gin.H{"code": 201, "msg": "密码修改失败"})
		}
	} else {
		ctx.JSON(http.StatusOK, gin.H{"code": 201, "msg": "原密码不正确，请重试！"})
	}
}
