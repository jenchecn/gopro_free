package menu

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"server/database/model"
	"strconv"
)

var menu model.Menu

func GetList(ctx *gin.Context) {
	result := menu.GetMenuTree(0)
	if result != nil {
		ctx.JSON(http.StatusOK, gin.H{"code": 200, "msg": "获取成功！", "list": result})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"code": 201, "msg": "数据为空！"})
	}
}
func GetMenuTree(ctx *gin.Context) {
	data := menu.GetRouteMenuTree()
	if data != nil {
		ctx.JSON(http.StatusOK, gin.H{"code": 200, "msg": "获取成功！", "data": data})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"code": 201, "msg": "数据为空！"})
	}
}
func GetOne(ctx *gin.Context) {
	result, err := menu.GetOne(0)
	if err == nil {
		ctx.JSON(http.StatusOK, gin.H{"code": 200, "msg": "获取成功！", "data": result})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"code": 201, "msg": "数据为空！"})
	}
}
func Add(ctx *gin.Context) {
	parentID, _ := strconv.Atoi(ctx.PostForm("parent_id"))
	menu = model.Menu{}
	_type, _ := strconv.Atoi(ctx.PostForm("type"))
	menu.Type = _type
	menu.ParentID = int64(parentID)
	menu.RoutePath = ctx.PostForm("route_path")
	menu.AssemblyPath = ctx.PostForm("assembly_path")
	menu.MenuName = ctx.PostForm("menu_name")
	menu.Icon = ctx.PostForm("icon")
	menu.Permission = ctx.PostForm("permission")
	err := menu.Add(menu)
	if err == nil {
		ctx.JSON(http.StatusOK, gin.H{"code": 200, "msg": "添加成功！"})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"code": 201, "msg": "添加失败"})
	}
}
func Edit(ctx *gin.Context) {
	id, _ := strconv.Atoi(ctx.PostForm("id"))
	menu = model.Menu{}
	menu.ID = uint(id)
	menu.MenuName = ctx.PostForm("menu_name")
	menu.Icon = ctx.PostForm("icon")
	err := menu.Edit(menu)
	if err == nil {
		ctx.JSON(http.StatusOK, gin.H{"code": 200, "msg": "修改成功！"})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"code": 201, "msg": "修改失败"})
	}
}
func Del(ctx *gin.Context) {
	id, _ := strconv.Atoi(ctx.PostForm("id"))
	err := menu.Del(uint(id))
	if err == nil {
		ctx.JSON(http.StatusOK, gin.H{"code": 200, "msg": "删除成功！"})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"code": 201, "msg": "删除失败"})
	}
}
func AddAll(ctx *gin.Context) {
	parentID, _ := strconv.Atoi(ctx.PostForm("parent_id"))
	permission := ctx.PostForm("permission")
	menus := []model.Menu{{
		Type:       2,
		ParentID:   int64(parentID),
		MenuName:   "查询",
		Permission: permission + ".query",
		BaseModel:  model.BaseModel{ID: 0, Status: 0, IsDelete: 0, CreatedAt: 0, UpdatedAt: 0},
	}, {
		Type:       2,
		ParentID:   int64(parentID),
		MenuName:   "新增",
		Permission: permission + ".add",
		BaseModel:  model.BaseModel{ID: 0, Status: 0, IsDelete: 0, CreatedAt: 0, UpdatedAt: 0},
	}, {
		Type:       2,
		ParentID:   int64(parentID),
		MenuName:   "编辑",
		Permission: permission + ".edit",
		BaseModel:  model.BaseModel{ID: 0, Status: 0, IsDelete: 0, CreatedAt: 0, UpdatedAt: 0},
	}, {
		Type:       2,
		ParentID:   int64(parentID),
		MenuName:   "删除",
		Permission: permission + ".del",
		BaseModel:  model.BaseModel{ID: 0, Status: 0, IsDelete: 0, CreatedAt: 0, UpdatedAt: 0},
	}}
	err := menu.AddAll(menus)
	if err == nil {
		ctx.JSON(http.StatusOK, gin.H{"code": 200, "msg": "批量添加成功！"})
	} else {
		ctx.JSON(http.StatusOK, gin.H{"code": 201, "msg": "批量添加失败"})
	}
}
