var m=Object.defineProperty;var i=Object.getOwnPropertySymbols;var c=Object.prototype.hasOwnProperty,l=Object.prototype.propertyIsEnumerable;var a=(e,s,t)=>s in e?m(e,s,{enumerable:!0,configurable:!0,writable:!0,value:t}):e[s]=t,n=(e,s)=>{for(var t in s||(s={}))c.call(s,t)&&a(e,t,s[t]);if(i)for(var t of i(s))l.call(s,t)&&a(e,t,s[t]);return e};import{u}from"./vue-router.bfa4d852.js";import{_ as j,u as f}from"./index.c5fd478c.js";import{v as d,y as b,c as h,w as v,a4 as g,z as y,A as k,K as p,E as w,k as x}from"./@vue.acf80055.js";import"./vuex.172edaaa.js";import"./nprogress.1043e533.js";import"./axios.19d3e859.js";import"./element-plus.e9dc1f18.js";import"./@vueuse.b952a4ba.js";import"./lodash.c4efa5f2.js";import"./resize-observer-polyfill.8deb1e21.js";import"./@popperjs.a8a4a6a0.js";import"./dayjs.fa1a2732.js";import"./async-validator.320b4077.js";import"./url-regex.869d4c49.js";import"./ip-regex.15dc1658.js";import"./tlds.298d0c99.js";import"./memoize-one.38c86d42.js";import"./normalize-wheel-es.2002d17b.js";import"./qs.17393965.js";import"./side-channel.fbfe5e96.js";import"./get-intrinsic.1675b33e.js";import"./has-symbols.caae0f97.js";import"./function-bind.cb3858f2.js";import"./has.c1051c46.js";import"./call-bind.0b5a4f26.js";import"./object-inspect.d28bfe1e.js";import"./moment.08a7f518.js";import"./vue-clipboard3.597571a5.js";import"./clipboard.3a38b97c.js";import"./mitt.550594b0.js";import"./vue-i18n.4ac7617b.js";import"./@intlify.879ca0b6.js";const _=d({name:"layoutLinkView",setup(){const e=u(),s=f(),t=b({currentRouteMeta:{}}),r=h(()=>{let{isTagsview:o}=s.state.themeConfig.themeConfig;return o?"114px":"80px"});return v(()=>e.path,()=>{t.currentRouteMeta=e.meta},{immediate:!0}),n({setLinkHeight:r},g(t))}}),R=["href"];function z(e,s,t,r,o,L){return x(),y("div",{class:"layout-view-bg-white flex layout-view-link",style:w({height:`calc(100vh - ${e.setLinkHeight}`})},[k("a",{href:e.currentRouteMeta.isLink,target:"_blank",rel:"opener",class:"flex-margin"},p(e.currentRouteMeta.title)+"\uFF1A"+p(e.currentRouteMeta.isLink),9,R)],4)}var ne=j(_,[["render",z]]);export{ne as default};
