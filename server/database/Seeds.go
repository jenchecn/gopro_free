/**
 * @Author: 490912587@qq.com
 * @Description:
 * @File:  Seeds
 * @Version: 1.0.0
 * @Date: 2021/11/11 10:24
 */

package database

import (
	"fmt"
	"server/database/model"
	"server/tool"
	"time"

	"gorm.io/gorm"
)

func User(db *gorm.DB) {
	var users []model.User
	users = append(users,
		model.User{
			Account:     "admin",
			Password:    tool.Md5("123456"),
			Phone:       "13000000000",
			NickName:    "超级管理员",
			Avatar:      "https://img1.baidu.com/it/u=3081151730,4195482971&fm=26&fmt=auto",
			Sex:         0,
			Email:       "490912587@qq.com",
			StoreId:     0,
			RoleId:      1,
			LastLoginIp: "127.0.0.1",
			Birthday:    time.Now().String()})
	if err := db.Create(&users).Error; err != nil {
		fmt.Println("User表数据Seed失败！", err)
	} else {
		fmt.Print("User表数据Seed完成！\r\n")
	}
}
func Menu() model.Menu {
	return model.Menu{}
}
