package database

import (
	"bytes"
	"fmt"
	"server/database/model"
	"server/tool"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func Init() {
	config := tool.Config()
	configStr := bytes.Buffer{}
	configStr.WriteString(config.Mysql.User + ":")
	configStr.WriteString(config.Mysql.Password + "@tcp(")
	configStr.WriteString(config.Mysql.Host + ":" + config.Mysql.Port + ")/" + config.Mysql.DbName)
	configStr.WriteString("?charset=utf8mb4&parseTime=True&loc=Local")
	db, err := gorm.Open(mysql.Open(configStr.String()))
	mySQL, err := db.DB()
	if err != nil {
		defer mySQL.Close()
		fmt.Println(err)
	}
	//设置最大空闲连接
	mySQL.SetMaxIdleConns(10)
	//设置最大连接数
	mySQL.SetMaxOpenConns(100)
	//设置连接超时时间:1分钟
	mySQL.SetConnMaxLifetime(time.Minute)
	//总平台
	db.AutoMigrate(
		&model.User{},
		&model.Menu{},
		&model.Role{},
		&model.RolePermission{})
	model.GlobalDB = db
	// 调试模式不需要填充数据库数据
	// 插入的数据目前使用的sql文件，可以自己seed默认数据
	// if !config.Debug {
	// 	Seed(db)
	// }
	fmt.Printf("数据库初始化成功！\r\n")
}

func Seed(db *gorm.DB) {
	User(db)
}
