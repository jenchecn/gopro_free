package model

import (
	"gorm.io/gorm"
	"time"
)

type BaseModel struct {
	ID        uint  `gorm:"primary_key"`
	Status    int   `gorm:"default:0;comment:'状态'"`
	IsDelete  int   `gorm:"default:0;comment:'是否假删除'"`
	CreatedAt int64 `gorm:"default:0;comment:'创建时间'"`
	UpdatedAt int64 `gorm:"default:0;comment:'更新时间'"`
}

var GlobalDB *gorm.DB

func (base *BaseModel) BeforeCreate(tx *gorm.DB) (err error) {
	base.CreatedAt = time.Now().Unix()
	base.UpdatedAt = time.Now().Unix()
	return
}
func (base *BaseModel) BeforeSave(tx *gorm.DB) (err error) {
	base.UpdatedAt = time.Now().Unix()
	return
}
func (base *BaseModel) BeforeUpdate(tx *gorm.DB) (err error) {
	base.UpdatedAt = time.Now().Unix()
	return
}
