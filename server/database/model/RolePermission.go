package model

import "server/tool"

type RolePermission struct {
	BaseModel
	RoleID int `gorm:"comment:'角色关联ID'";json:"role_id"`
	Permission string `gorm:"comment:'角色权限标识";json:"permission"`
}

var rolePermission RolePermission
var rolePermissions []RolePermission
func (e *RolePermission) GetList(param tool.Pagination) ([]RolePermission, error) {
	if err := GlobalDB.Limit(param.PageSize).Offset((param.PageNo - 1) * param.PageSize).Find(&roles).Error; err != nil {
		return rolePermissions, err
	}
	return rolePermissions, nil
}
func (e *RolePermission) GetPermissionsByRoleID(id uint) ([]RolePermission, error) {
	if err := GlobalDB.Where("role_id = ?", id).Find(&rolePermissions).Error; err != nil {
		return rolePermissions, err
	}
	return rolePermissions, nil
}
func (e *RolePermission) Add(model RolePermission)  error {
	if err := GlobalDB.Create(&model).Error; err != nil {
		return err
	}
	return nil
}
func (e *RolePermission) Edit(model RolePermission) error {
	if err := GlobalDB.Updates(&model).Error; err != nil {
		return  err
	}
	return nil
}
func (e *RolePermission) Del(id uint) error {
	if err := GlobalDB.Delete(id).Error; err != nil {
		return err
	}
	return nil
}
