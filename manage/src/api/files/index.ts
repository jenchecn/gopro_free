import request from '/@/utils/request';
import Qs from 'qs'
export function getList(params: object) {
	return request({
		url: '/basic/files/list',
		method: 'get',
		params,
	});
}
export function upload(params: object) {
	return request({
		url: '/basic/files/upload',
		method: 'post',
		data: Qs.stringify(params)
	});
}
export function uploads(params: object) {
	return request({
		url: '/basic/files/uploads',
		method: 'post',
		data: Qs.stringify(params)
	});
}
export function add(params: object) {
	return request({
		url: '/basic/files/add',
		method: 'post',
		data: Qs.stringify(params)
	});
}
export function edit(params: object) {
	return request({
		url: '/basic/files/edit',
		method: 'post',
		data: Qs.stringify(params)
	});
}