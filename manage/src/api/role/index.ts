import request from '/@/utils/request';
import Qs from 'qs'
export function getList(params: object) {
	return request({
		url: '/basic/role/list',
		method: 'get',
		params,
	});
}
export function add(params: object) {
	return request({
		url: '/basic/role/add',
		method: 'post',
		data: Qs.stringify(params)
	});
}
export function edit(params: object) {
	return request({
		url: '/basic/role/edit',
		method: 'post',
		data: Qs.stringify(params)
	});
}