import request from '/@/utils/request';
import Qs from 'qs'
export function getList(params: object) {
	return request({
		url: '/basic/user/list',
		method: 'get',
		params,
	});
}
export function add(params: object) {
	return request({
		url: '/basic/user/add',
		method: 'post',
		data: Qs.stringify(params)
	});
}
export function edit(params: object) {
	return request({
		url: '/basic/user/edit',
		method: 'post',
		data: Qs.stringify(params)
	});
}
export function change_pass(params: object) {
	return request({
		url: '/basic/user/change_pass',
		method: 'post',
		data: Qs.stringify(params)
	});
}