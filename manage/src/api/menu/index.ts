import request from '/@/utils/request';
import Qs from 'qs'

export function getMenuTree(params: object) {
	return request({
		url: '/basic/menu/menu_tree',
		method: 'get',
		params,
	});
}
export function getList(params: object) {
	return request({
		url: '/basic/menu/list',
		method: 'get',
		params,
	});
}
export function add(params: object) {
	return request({
		url: '/basic/menu/add',
		method: 'post',
		data: Qs.stringify(params)
	});
}
export function edit(params: object) {
	return request({
		url: '/basic/menu/edit',
		method: 'post',
		data: Qs.stringify(params)
	});
}
export function add_all(params: object) {
	return request({
		url: '/basic/menu/add_all',
		method: 'post',
		data: Qs.stringify(params)
	});
}