import request from '/@/utils/request';
import Qs from 'qs'
/**
 * 用户登录
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function signIn(params: object) {
	return request({
		url: '/password_login',
		method: 'post',
		data: Qs.stringify(params)
	});
}

/**
 * 用户退出登录
 * @param params 要传的参数值
 * @returns 返回接口数据
 */
export function signOut(params: object) {
	return request({
		url: '/user/signOut',
		method: 'post',
		data: Qs.stringify(params)
	});
}
export function getCaptcha(params: object) {
	return request({
		url: '/getCaptcha',
		method: 'get'
	});
}
export function passwordLoginOut(params: object) {
	return request({
		url: '/password_login_out',
		method: 'get'
	});
}
