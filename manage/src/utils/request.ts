import axios from 'axios';
import { ElMessage, ElMessageBox } from 'element-plus';
import { Session } from '/@/utils/storage';

// 配置新建一个 axios 实例
const service = axios.create({
	baseURL: import.meta.env.VITE_API_URL as any,
	timeout: 50000,
	headers: {
		"Content-Type": "application/x-www-form-urlencoded"
	}
});

// 添加请求拦截器
service.interceptors.request.use(
	(config) => {
		// 在发送请求之前做些什么 token
		if (Session.get('token')) {
			config.headers.common['Authorization'] = `${Session.get('token')}`;
		}
		return config;
	},
	(error) => {
		// 对请求错误做些什么
		return Promise.reject(error);
	}
);

// 添加响应拦截器
service.interceptors.response.use(
	(response) => {
		// 对响应数据做点什么
		const res = response.data;
		if (res.code && res.code !== 200) {
			// 201为业务错误码，和httpstatus无关
			if (res.code === 201) {
				ElMessageBox.alert(res.msg, '提示', {}).then(() => { }).catch(() => { });
				return res;
			}
			if (res.code === 401) {
				Session.clear(); // 清除浏览器全部临时缓存
				ElMessageBox.alert(res.msg, '提示', {})
					.then(() => {
						window.location.href = '/public/'; // 去登录页
					})
					.catch(() => { });
			}
			return Promise.reject(service.interceptors.response);
		} else {
			return res;
		}
	},
	(error) => {
		 Session.clear(); // 清除浏览器全部临时缓存
		 ElMessageBox.alert(error, '提示', {}).then(() => {
			 window.location.href = '/public/'; // 去登录页
			}).catch(() => { });
		return Promise.reject(error);
	}
);

// 导出 axios 实例
export default service;
